<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    use HasFactory;

    
    protected $table = 'alumnos';
    public $primaryKey = 'id_alumno';

    protected $fillable = [
        'nombre_alumno',
        'apellido_alumno',
        'direccion_alumno',
        'telefono_alumno',
        'fechaingreso_alumno'
    ];
}
