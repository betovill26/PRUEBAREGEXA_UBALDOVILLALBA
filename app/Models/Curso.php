<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $table = 'cursos';

    public $primaryKey = 'id_curso';

    public $timestamps = false;

    protected $fillable = [
        'nom_curso',
        'alumnos_id_alumno'
    ];
}
