<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CursoController extends Controller
{
    
    public function index()
    {  
        $cursos = Curso::all();

         return Inertia::render('Cursos/Listado', ['cursos' => $cursos]); 
        
    }

    
    public function create()
    {
       
        return Inertia::render('Cursos/FormNuevo');
    }

  
    public function store(Request $request)
    {
        $request->validate([
            'nom_curso',
            'alumnos_id_alumno'
        ]);

        Curso::create($request->all());

        return Redirect::route('cursos.index');
    }

  
    public function show(Curso $curso)
    {
        //
    }

   
    public function edit($id)
    {
        $cursos = Curso::find($id);
        return Inertia::render('Cursos/FormEditar', 
        ['cursos' => $cursos->only('id_curso', 'nom_curso', 'alumnos_id_alumno')]);
    }

    
    public function update(Request $request, Curso $curso)
    {
        $request->validate([
            'nom_curso',
            'alumnos_id_alumno'
        ]);

        $curso->where('id_curso', $request->id_curso)->update($request->only('nom_curso', 'alumnos_id_alumno'));

        return Redirect::route('cursos.index');

    }

    
    public function destroy($id)
    {
        $cursos = Curso::find($id);
        $cursos->delete();

        return Redirect::route('cursos.index');
    }
}
